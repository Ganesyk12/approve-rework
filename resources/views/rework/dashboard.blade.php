@extends('app')

@section('title', 'REWORK')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="pagetitle">
      <h1>Rework Report</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html">Home</a></li>
          <li class="breadcrumb-item active">Rework</li>
        </ol>
      </nav>
    </div>
    <div class="row mb-2 mb-3">
      <div class="col-md-3">
        <div class="input-group">
          <span class="input-group-text col-md-6" for="src_createdAt">Tanggal Dibuat</span>
          <input type="date" class="form-control" id="src_createdAt">
        </div>
      </div>
      <div class="col-md-2 offset-7">
        <button type="button" class="btn btn-block btn-warning" style="width: 100%" onclick="resetFilter()"><i
            class="bx bx-reset"></i> Reset
          Search</button>
      </div>
    </div>
    <div class="row mb-3">
      <div class="col-md-3">
        <div class="input-group">
          <span class="input-group-text col-md-6" for="src_partName">Part Name</span>
          <input type="text" id="src_partName" class="form-control" placeholder="Insert PartName">
        </div>
      </div>
      <div class="col-md-4">
        <div class="input-group">
          <span class="input-group-text col-md-6" for="src_partNo">Part Number</span>
          <input type="text" id="src_partNo" class="form-control" placeholder="Insert PartNumber">
        </div>
      </div>
      <div class="col-md-3">
        <div class="input-group">
          <span class="input-group-text col-md-6" for="src_lotProd">Lot Produksi</span>
          <input type="text" id="src_lotProd" class="form-control" placeholder="Insert LotProduksi">
        </div>
      </div>
      <div class="col-md-2">
        <button type="button" class="btn btn-block btn-primary" data-bs-toggle="modal" data-bs-target="#AddModal"
          style="width: 100%" onclick="view_modal('1','Add')"><i class="ri-add-circle-line"
            style="font-size: 15px;"></i> Add Data</button>
      </div>
    </div>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">TABEL REWORK</h5>
          <table class="table table-hover table-responsive" id="tabelform" style="font-size: 15px;">
            <thead>
              <tr>
                <th>Tanggal</th>
                <th>Part Name</th>
                <th>Part Number</th>
                <th>Lot Produksi</th>
                <th>Customer</th>
                <th>Anoth.Customer</th>
                <th>Problem</th>
                <th>Request to</th>
                <th>Recipient Name</th>
                <th>PIC's Sortir</th>
                <th>Prepared by</th>
                <th>Approved by</th>
                <th>Status Sortir</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

{{-- Modal Add --}}
<div class="modal fade" id="AddModal" tabindex="-1">
  <div class="modal-dialog modal-xl modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titlemodal">Base Modal</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        {{-- Form Utama --}}
        <form role="form" id="quickForm">
          <div class="row g-3 mb-3" hidden>
            <div class="col">
              <div class="input-group">
                <label for="hdrid" class="input-group-text">HDRID</label>
                <input type="text" class="form-control" id="hdrid">
              </div>
            </div>
          </div>
          <div class="row g-3 mb-3">
            <div class="col-md-6">
              <div class="input-group">
                <label for="part_number" class="input-group-text col-md-4">Part Number</label>
                <select class="form-select select2 text-center" id="part_number" name="part_number"
                  onchange="handleSelectChange_part_number(event)" style="width: auto;">
                  <option value='' selected="selected">- Select Part Number-</option>
                  <option value=''> Other Part Number </option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="input-group">
                <label for="part_name" class="input-group-text col-md-4">Part Name</label>
                <input type="text" class="form-control text-center" id="part_name" disabled>
              </div>
            </div>
          </div>
          <div class="row g-3 mb-3">
            <div class="col-md-6">
              <div class="input-group">
                <label for="cust_name" class="input-group-text col-md-4">Customer</label>
                <select class="form-select select2 text-center" id="cust_name" name="cust_name"
                  onchange="handleSelectChange_cust_name(event)" style="width: auto;">
                  <option value='' selected="selected">- Select Customer -</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="input-group">
                <label for="anothercust_name" class="input-group-text col-md-4">Another Customer</label>
                <select class="form-select select2 text-center" id="anothercust_name" name="anothercust_name"
                  onchange="handleSelectChange_anothercust_name(event)" style="width: auto;">
                  <option value='' selected="selected">- Select Another Customer -</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row g-3 mb-3">
            <div class="col-md-12">
              <div class="input-group">
                <label for="problem" class="input-group-text col-md-2">Problem</label>
                <textarea name="" id="problem" rows="1" class="form-control"></textarea>
              </div>
            </div>
          </div>
          <div class="row g-3 mb-3">
            <div class="col-md-6">
              <div class="input-group">
                <label for="occ_place" class="input-group-text col-md-4">Occ Place</label>
                <textarea name="occ_place" id="occ_place" rows="1" class="form-control"></textarea>
              </div>
            </div>
            <div class="col-md-6">
              <div class="input-group">
                <label for="occ_date" class="input-group-text col-md-4">Occ Date</label>
                <input type="date" class="form-control" id="occ_date">
              </div>
            </div>
          </div>
          <div class="row g-3 mb-3">
            <div class="col-md-6">
              <div class="input-group">
                <label for="ng_qty" class="input-group-text col-md-4">NG QTY</label>
                <input type="number" min="1" class="form-control" id="ng_qty">
              </div>
            </div>
            <div class="col-md-6">
              <div class="input-group">
                <label for="defect_lot" class="input-group-text col-md-4">Defect Lot</label>
                <textarea name="defect_lot" id="defect_lot" rows="1" class="form-control"></textarea>
              </div>
            </div>
          </div>
          <div class="row g-3 mb-3">
            <div class="col-md-12">
              <div class="input-group">
                <label for="area" class="input-group-text col-md-2">Area</label>
                <textarea name="area" id="area" rows="1" class="form-control"></textarea>
              </div>
            </div>
          </div>
          <div class="row g-3">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Sketch</h5>
                <div class="row">
                  <div class="col-md-4">
                    <div class="card">
                      <div class="card-body">
                        <h5 class="card-title" for="sketch1">Sketch 1</h5>
                        <input class="form-control mb-3" type="file" id="sketch1">
                        <textarea name="deskripsi1" id="deskripsi1" rows="1" class="form-control"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="card">
                      <div class="card-body">
                        <h5 class="card-title" for="sketch2">Sketch 2</h5>
                        <input class="form-control mb-3" type="file" id="sketch2">
                        <textarea name="deskripsi2" id="deskripsi2" rows="1" class="form-control"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="card">
                      <div class="card-body">
                        <h5 class="card-title" for="sketch3">Sketch 3</h5>
                        <input class="form-control mb-3" type="file" id="sketch3">
                        <textarea name="deskripsi3" id="deskripsi3" rows="1" class="form-control"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row g-3 mb-3">
            <div class="col-md-12">
              <div class="input-group">
                <label for="checking_method" class="input-group-text col-md-2">Checking Method</label>
                <textarea name="checking_method" id="checking_method" rows="1" class="form-control"></textarea>
              </div>
            </div>
          </div>
          <div class="row g-3 mb-3">
            <div class="col-md-8 offset-4">
              <div class="input-group">
                <label for="attach_ik" class="input-group-text">Attach IK</label>
                <input class="form-control" type="file" id="attach_ik">
              </div>
            </div>
          </div>
          <div class="row g-3">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Marking</h5>
                <div class="row">
                  <div class="col-md-6">
                    <div class="card">
                      <div class="card-body">
                        <h5 class="card-title" for="marking_photo">Marking Photo</h5>
                        <input class="form-control mb-3" type="file" id="marking_photo">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="card">
                      <div class="card-body">
                        <h5 class="card-title" for="marking">Description Marking</h5>
                        <textarea name="marking" id="marking" rows="1" class="form-control mb-3"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row g-3 mb-3">
            <label class="col-sm-1 col-form-label">1.</label>
            <label class="col-sm-8 col-form-label" for="sortir">Apakah produk yang sejenis juga perlu disortir? (Jika
              Ya,isilah dengan part no. yang sejenis)</label>
            <div class="col-md-2 mt-3">
              <div class="form-group clearfix d-flex gap-3 mt-2 justify-content-center">
                <div class="icheck-primary d-inline">
                  <input class="form-check-input" type="radio" id="sortir_ada" name="sortir" value="YA">
                  <label for="sortir_ada">YA</label>
                </div>
                <div class="icheck-primary d-inline ml-auto">
                  <input class="form-check-input" type="radio" id="sortir_tidak" name="sortir" value="TIDAK">
                  <label for="sortir_tidak">TIDAK</label>
                </div>
              </div>
            </div>
          </div>

          {{-- FORM SORTIR --}}
          <div class="row g-3 mb-3">
            <div class="card" id="detailSortir">
              <div class="card-body">
                <h5 class="card-title">SORTIR REQUEST</h5>
              </div>
              <form role="form" id="formSortir">
                <div class="card-body">
                  <div class="form-group">
                    <div class="card-header flex-row d-flex gap-3 overflow-auto mb-3">
                      <div class="col-md-4" hidden>
                        <div class="input-group">
                          <label for="id_sortir" class="input-group-text">ID SORTIR</label>
                          <input name="id_sortir" id="id_sortir" class="form-control"></input>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="input-group">
                          <label for="part_numberSortir" class="input-group-text">Part Number</label>
                          <input name="part_number" id="part_numberSortir" class="form-control"></input>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="input-group">
                          <label for="lot_produksiSortir" class="input-group-text">Lot Produksi</label>
                          <input name="lot_produksi" id="lot_produksiSortir" class="form-control" disabled></input>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="input-group">
                          <label for="customerSortir" class="input-group-text">Customer</label>
                          <input name="customer" id="customerSortir" class="form-control"></input>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <label class="form-control overflow-auto">Apakah lot bermasalah ada stock di WH?</label>
                        <div class="form-group clearfix d-flex gap-5 justify-content-center">
                          <div class="icheck-primary d-inline">
                            <input class="form-check-input" type="radio" id="masalah_stok_ada" name="masalah_stok"
                              value="YA">
                            <label for="masalah_stok_ada">
                              YA
                            </label>
                          </div>
                          <div class="icheck-primary d-inline ml-auto">
                            <input class="form-check-input" type="radio" id="masalah_stok_tidak" name="masalah_stok"
                              value="TIDAK">
                            <label for="masalah_stok_tidak">
                              TIDAK
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <label class="form-control">Apakah lot bermasalah sedang terkirim ke
                          customer?</label>
                        <div class="form-group clearfix d-flex gap-5 justify-content-center">
                          <div class="icheck-primary d-inline">
                            <input class="form-check-input" type="radio" id="masalah_customer_ada"
                              name="masalah_customer" value="YA">
                            <label for="masalah_customer_ada">
                              YA
                            </label>
                          </div>
                          <div class="icheck-primary d-inline ml-auto">
                            <input class="form-check-input" type="radio" id="masalah_customer_tidak"
                              name="masalah_customer" value="TIDAK">
                            <label for="masalah_customer_tidak">
                              TIDAK
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="input-group">
                          <label for="tgl_berangkat_cust" class="input-group-text">Tanggal Keberangkatan (Dari
                            DMIA)</label>
                          <input type="date" name="tgl_berangkat_cust" id="tgl_berangkat_cust"
                            class="form-control"></input>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group">
                          <label for="tgl_datang_cust" class="input-group-text">Tanggal Kedatangan (Sampai di
                            Customer)</label>
                          <input type="date" name="tgl_datang_cust" id="tgl_datang_cust" class="form-control"></input>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <label class="form-control">Apakah lot bermasalah ada stock di Yusen?</label>
                        <div class="form-group clearfix d-flex gap-5 justify-content-center">
                          <div class="icheck-primary d-inline">
                            <input class="form-check-input" type="radio" id="masalah_yusen_ada" name="masalah_yusen"
                              value="YA">
                            <label for="masalah_yusen_ada">
                              YA
                            </label>
                          </div>
                          <div class="icheck-primary d-inline ml-auto">
                            <input class="form-check-input" type="radio" id="masalah_yusen_tidak" name="masalah_yusen"
                              value="TIDAK">
                            <label for="masalah_yusen_tidak">
                              TIDAK
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="input-group">
                          <label for="tgl_berangkat_yusen" class="input-group-text">Tanggal Keberangkatan (Dari
                            DMIA)</label>
                          <input type="date" name="tgl_berangkat_yusen" id="tgl_berangkat_yusen"
                            class="form-control"></input>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="input-group">
                          <label for="tgl_datang_yusen" class="input-group-text">Tanggal Kedatangan (Sampai di
                            Customer)</label>
                          <input type="date" name="tgl_datang_yusen" id="tgl_datang_yusen" class="form-control"></input>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="input-group">
                          <label for="qty" class="input-group-text">QTY</label>
                          <input type="number" min="1" name="qty" id="qty" class="form-control"></input>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>

              <div class="col-md-4 mb-3">
                <button type="button" id="simpansortir" class="btn btn-sm btn-primary">Simpan Data ke Tabel
                  Sortir</button>
              </div>
              <div class="card-body">
                <table class="table table-hover table-responsive text-center" id="tabelSortir" style="font-size: 15px;">
                  <thead>
                    <tr>
                      <th>Action</th>
                      <th>Part Number</th>
                      <th>Lot Produksi</th>
                      <th>Customer</th>
                      <th>QTY</th>
                      <th>Masalah Stock WH</th>
                      <th>Masalah Lot Customer</th>
                      <th>Tanggal Berangkat Cust</th>
                      <th>Tanggal Datang Cust</th>
                      <th>Masalah Stock Yusen</th>
                      <th>Tanggal Berangkay Yusen</th>
                      <th>Tanggal Datang Yusen</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="row g-3 mb-3">
            <label class="col-sm-1 col-form-label">2.</label>
            <label class="col-sm-8 col-form-label" for="perubahan_sortir">Apakah ada perubahan
              Spec. std, packaging, Marking, Label, dll. Selama Sortir? (Jika
              Ya, isilah input kolom dibawah ini)</label>
            <div class="col-md-2 mt-3">
              <div class="form-group clearfix d-flex gap-3 mt-2 justify-content-center">
                <div class="icheck-primary d-inline">
                  <input class="form-check-input" type="radio" id="perubahan_sortir_ada" name="perubahan_sortir"
                    value="YA">
                  <label for="perubahan_sortir_ada">
                    YA
                  </label>
                </div>
                <div class="icheck-primary d-inline ml-auto">
                  <input class="form-check-input" type="radio" id="perubahan_sortir_tidak" name="perubahan_sortir"
                    value="TIDAK">
                  <label for="perubahan_sortir_tidak">
                    TIDAK
                  </label>
                </div>
              </div>
            </div>
          </div>

          {{-- FORM PERUBAHAN --}}
          <div class="row g-3 mb-3">
            <div class="card" id="detailChange">
              <div class="card-body">
                <h5 class="card-title">CHANGE SORTIR</h5>
              </div>
              <form role="form" id="formChange">
                <div class="card-body">
                  <div class="form-group">
                    <div class="card-header flex-row d-flex gap-3 overflow-auto mb-3">
                      <div class="col-md-4" hidden>
                        <div class="input-group">
                          <label for="id_change" class="input-group-text">ID CHANGE SORTIR</label>
                          <input name="id_change" id="id_change" class="form-control"></input>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="input-group">
                          <label for="tgl_perubahan" class="input-group-text">Tanggal Perubahan</label>
                          <input type="date" name="tgl_perubahan" id="tgl_perubahan" class="form-control"></input>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="input-group">
                          <label for="jenis_perubahan" class="input-group-text">Jenis Perubahan</label>
                          <input name="jenis_perubahan" id="jenis_perubahan" class="form-control"></input>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="input-group">
                          <label for="check_spv" class="input-group-text">Check SPV</label>
                          <input name="check_spv" id="check_spv" class="form-control" disabled></input>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="input-group">
                          <label for="check_mgr" class="input-group-text">Check MGR</label>
                          <input name="check_mgr" id="check_mgr" class="form-control" disabled></input>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
              <div class="col-md-4 mb-3">
                <button type="button" id="simpan_chgsortir" class="btn btn-sm btn-primary">Simpan Data ke Tabel
                  Perubahan</button>
              </div>
              <div class="card-body">
                <table class="table table-hover table-responsive text-center" id="tabelChange" style="font-size: 15px;">
                  <thead>
                    <tr>
                      <th>Action</th>
                      <th>Tanggal Perubahan</th>
                      <th>Jenis Perubahan</th>
                      <th>Check SPV</th>
                      <th>Check MGR</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="row g-3 mb-5">
            <div class="col-md-4">
              <label for="request_to" class="form-control text-center">Request to</label>
            </div>
            <div class="col-md-4">
              <div class="form-group clearfix d-flex gap-5 mt-2 justify-content-center">
                <div class="icheck-primary d-inline">
                  <input class="form-check-input" type="radio" id="request_qc" name="request_to" value="QC"
                    onchange="handleRadioChange(event)">
                  <label for="request_qc">
                    QC
                  </label>
                </div>
                <div class="icheck-primary d-inline ml-auto">
                  <input class="form-check-input" type="radio" id="request_mfg" name="request_to" value="MFG"
                    onchange="handleRadioChange(event)">
                  <label for="request_mfg">
                    MFG
                  </label>
                </div>
              </div>
            </div>
            <div class="col-md-4" id="kolom_qc">
              <select class="form-select select2 text-center" id="select_qc" name="nama_recipient"
                onchange="handleSelectChange_qc(event)" style="width: 100%;">
                <option value='' selected="selected">- Select QC -</option>
              </select>
            </div>
            <div class="col-md-4" id="kolom_mgr">
              <select class="form-select select2 text-center" id="select_mfg" name="nama_recipient"
                onchange="handleSelectChange_mfg(event)" style="width: 100%;">
                <option value='' selected="selected">- Select MFG -</option>
              </select>
            </div>
          </div>

          <div class="row g-3 mb-3">
            <div class="col-md-4">
              <label for="request_by" class="form-control text-center">REQUEST BY</label>
            </div>
            <div class="col-md-4">
              <input name="request_by" id="request_by" class="form-control" disabled>
            </div>
            <div class="col-md-4">
              <input name="role_request" id="role_request" class="form-control" disabled>
            </div>
          </div>

          <div class="row g-3 mb-3">
            <div class="col-md-4">
              <label for="checked_by" class="form-control text-center">CHECKED BY</label>
            </div>
            <div class="col-md-4">
              <input name="checked_by" id="checked_by" class="form-control" disabled>
            </div>
            <div class="col-md-4">
              <input name="role_checked" id="role_checked" class="form-control" disabled>
            </div>
          </div>

          <div class="row g-3 mb-3">
            <div class="col-md-4">
              <label for="approved_by" class="form-control text-center">APPROVED BY</label>
            </div>
            <div class="col-md-4">
              <input name="approved_by" id="approved_by" class="form-control" disabled>
            </div>
            <div class="col-md-4">
              <input name="role_approved" id="role_approved" class="form-control" disabled>
            </div>
          </div>


        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="simpan_data" onclick="simpan_data('Add')">Button Save</button>
        <button type="button" class="btn btn-danger" id="reject_data">Button Reject</button>
        <button type="button" class="btn btn-primary" id="update_data" onclick="simpan_data('Update')">Button
          Edit</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

{{-- ========================================================================= --}}

<script>
  var table = null;
  var table2 = null;
  var table3 = null;

  $(document).ready(function() {
      table = $('#tabelform').DataTable({
      processing: true,
      serverSide: true,
      responsive: true,
      scrollX: true,
      ordering: true,
      order: [[0, 'desc']],
      ajax: "{{ route('data.get') }}",
      aLengthMenu: [
        [10, 25, 50, 100, 1000, 10000],
        [10, 25, 50, 100, 1000, "All"]
      ],
      columns: [
        { data: 'hdrid', visible: false },
        { data: 'transaction_date' },
        { data: 'part_name' },
        { data: 'part_number' },
        { data: 'lot_produksi' },
        { data: 'cust_name' },
        { data: 'anothercust_name' },
        { data: 'problem' },
        { data: 'request_to' },
        { data: 'nama_recipient' },
        { data: 'request_by' },
        { data: 'checked_by' },
        { data: 'approved_by' },
        { data: 'status_sortir' },
        { data: 'action', sortable: false, searchable: false, 
        render: function(data, type, row, meta) {
          mnu = '';
          mnu = mnu + '<a href="javascript:void(0)" class="btn btn-success btn-sm" title="View Data" onclick="view_modal(\''+row.hdrid+'\', \'View\')"><i class="ri-eye-fill"></i></a> ';
          mnu = mnu + '<a href="javascript:void(0)" class="btn btn-info btn-sm" title="Edit Data" onclick="view_modal(\''+row.hdrid+'\', \'Edit\')"><i class="ri-pencil-line"></i></a> ';
          mnu = mnu + '<a href="javascript:void(0)" class="btn btn-danger btn-sm" title="Delete Data" onclick="view_modal(\''+row.hdrid+'\', \'Delete\')"><i class="ri-delete-bin-5-fill"></i></a> ';
          return mnu;
        }},
      ],
    });

    table2 = $('#tabelSortir').DataTable({
      processing: true,
      serverSide: true,
      responsive: true,
      scrollX: true,
      ordering: true,
      order: [[0, 'desc']],
      ajax: "{{ route('data.get') }}",
      aLengthMenu: [
        [10, 25, 50, 100, 1000, 10000],
        [10, 25, 50, 100, 1000, "All"]
      ],
      columns: [
        { data: 'hdrid', visible: false },
      ],
    });

    table3 = $('#tabelChange').DataTable({
      processing: true,
      serverSide: true,
      responsive: true,
      scrollX: true,
      ordering: true,
      order: [[0, 'desc']],
      ajax: "{{ route('data.get') }}",
      aLengthMenu: [
        [10, 25, 50, 100, 1000, 10000],
        [10, 25, 50, 100, 1000, "All"]
      ],
      columns: [
        { data: 'hdrid', visible: false },
      ],
    });

  });
  

  function view_modal(hdrid, status){
    if (status == "Add") {
      $('#modal_view').modal('show');
      $('#titlemodal').text('Add Data Request');
      $('#quickForm')[0].reset();
      $('#formSortir')[0].reset();
      $('#formChange')[0].reset();
      $('#part_number').select2().val('');
      $('#cust_name').select2().val('');
      $('#anothercust_name').select2().val('');
      $('#select_qc').select2().val('');
      $('#select_mfg').select2().val('');
      document.getElementById("reject_data").style.visibility = 'hidden';
      document.getElementById("update_data").style.visibility = 'hidden';
    }else{
      $('hdrid').val(hdrid);
      var hdrid = hdrid;
      $.ajax({
        url = "{{ route('data.getbyhdrid') }}",
        method = "GET",
        data = {hdrid:hdrid},
        success:function(data){
          $('#modal_view').modal('show');
          $('#titlemodal').text('Update Data Request');
          $('#part_number').select2().val(data.part_number);
          $('#cust_name').select2().val(data.cust_name);
          $('#anothercust_name').select2().val(data.anothercust_name);
          $('#select_qc').select2().val(data.qc_name);
          $('#select_mfg').select2().val(data.mfg_name);
          document.getElementById("reject_data").style.visibility = 'visible';
          document.getElementById("update_data").style.visibility = 'visible';

          // Menggunakan operator ternary untuk menentukan apakah ada data, jika tidak, tampilkan "Upload"
					document.getElementById('sketch1_label').innerHTML = data.sketch1 ? data
						.sketch1 : 'No file choosen | Upload';
					document.getElementById('sketch2_label').innerHTML = data.sketch2 ? data
						.sketch2 : 'No file choosen | Upload';
					document.getElementById('sketch3_label').innerHTML = data.sketch3 ? data
						.sketch3 : 'No file choosen | Upload';
					document.getElementById('attach_ik_label').innerHTML = data.attach_ik ? data
						.attach_ik : 'No file choosen | Upload';
					document.getElementById('marking_photo_label').innerHTML = data
						.marking_photo ? data
						.marking_photo : 'No file choosen | Upload';

          if (data.sortir == 'YA') {
          document.getElementById("sortir_ada").checked = true;             
          }else{
          document.getElementById("sortir_tidak").checked = true;
          };

          if (data.change == 'YA') {
          document.getElementById("change_ada").checked = true;             
          }else{
          document.getElementById("change_tidak").checked = true;
          };
        }
      })
    }
  }
</script>

<script>
  function resetFilter() {
      $('#src_createdAt').val('');
      $('#src_partName').val('').trigger('change');
      $('#src_partNo').val('').trigger('change');
      $('#src_lotProd').val('');
  }

  $('#masalah_customer_ada').on('change', function() {
    $('#tgl_berangkat_cust').style('disabled', false);
    $('#tgl_datang_cust').style('disabled', false);
  })

  $('#masalah_customer_tidak').on('change', function() {
    $('#tgl_berangkat_cust').style('disabled', true);
    $('#tgl_datang_cust').style('disabled', true);
  })

  $('#masalah_yusen_ada').on('change', function() {
    $('#tgl_berangkat_yusen').prop('disabled', false);
    $('#tgl_datang_yusen').prop('disabled', false);
  })

  $('#masalah_yusen_tidak').on('change', function() {
    $('#tgl_berangkat_yusen').prop('disabled', true);
    $('#tgl_datang_yusen').prop('disabled', true);
  })

  $(document).ready(function() {
    function handleRadioChange(event) {
    const selectedValue = event.target.value;

    if (selectedValue === 'QC') {
      $('#kolom_qc').removeAttribute('hidden');
      $('#kolom_mgr').setAttribute('hidden', true);
    } else if (selectedValue === 'MFG') {
      $('#kolom_qc').setAttribute('hidden', true);
      $('#kolom_mgr').removeAttribute('hidden');
    }else{
      $('#kolom_qc').setAttribute('hidden', true);
      $('#kolom_mgr').setAttribute('hidden', true);
    }
  }
  
  })
  

</script>


<script>
  function handleSelectChange_qc(event) {
  // Your additional logic for QC select change event if needed
  }

  function handleSelectChange_mfg(event) {
  // Your additional logic for MFG select change event if needed
  }
</script>

@endsection