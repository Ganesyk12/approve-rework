<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="utf-8">
   <meta content="width=device-width, initial-scale=1.0" name="viewport">

   <title>@yield('title')</title>

   <meta content="" name="description">
   <meta content="" name="keywords">

   <!-- Favicons -->
   <link href="{{ asset('/img/favicon.png') }}" rel="icon">
   <link href="{{ asset('/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

   {{-- FontAwesome --}}
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

   <!-- Google Fonts -->
   <link href="https://fonts.gstatic.com" rel="preconnect">
   <link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i">

   <!-- Vendor CSS Files -->
   <link rel="stylesheet" href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css') }}">
   <link rel="stylesheet" href="{{ asset('/vendor/bootstrap-icons/bootstrap-icons.css') }}">
   <link rel="stylesheet" href="{{ asset('/vendor/boxicons/css/boxicons.min.css') }}">
   <link rel="stylesheet" href="{{ asset('/vendor/quill/quill.snow.css') }}">
   <link rel="stylesheet" href="{{ asset('/vendor/quill/quill.bubble.css') }}">
   <link rel="stylesheet" href="{{ asset('/vendor/remixicon/remixicon.css') }}">
   <link rel="stylesheet" href="{{ asset('/vendor/simple-datatables/style.css') }}">

   <!-- Template Main CSS File -->
   <link rel="stylesheet" href="{{ asset('/css/style.css') }}">

   {{-- JQuery --}}
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
   <script src="https://cdn.datatables.net/plug-ins/2.0.2/api/sum().js"></script>

</head>

<body class="toggle-sidebar">

   @include('master.header')
   @include('master.sidebar')
   <main id="main" class="main">
      @yield('content')
   </main>
   @include('master.footer')

   <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
         class="bi bi-arrow-up-short"></i></a>
   <!-- Vendor JS Files -->
   <script src="{{ asset('/vendor/apexcharts/apexcharts.min.js') }}"></script>
   <script src="{{ asset('/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
   <script src="{{ asset('/vendor/chart.js/chart.umd.js') }}"></script>
   <script src="{{ asset('/vendor/echarts/echarts.min.js') }}"></script>
   <script src="{{ asset('/vendor/quill/quill.js') }}"></script>
   <script src="{{ asset('/vendor/simple-datatables/simple-datatables.js') }}"></script>
   <script src="{{ asset('/vendor/tinymce/tinymce.min.js') }}"></script>
   <script src="{{ asset('/vendor/php-email-form/validate.js') }}"></script>
   <!-- Template Main JS File -->
   <script src="{{ asset('/js/main.js') }}"></script>

</body>

</html>