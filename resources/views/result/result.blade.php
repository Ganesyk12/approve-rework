@extends('app')

@section('title', 'RESULT')
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">RESULT</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item">Dashboard</li>
          <li class="breadcrumb-item active">RESULT</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <table class="table-responsive table-bordered">
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection