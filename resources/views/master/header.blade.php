<header id="header" class="header fixed-top d-flex align-items-center bg-dark-subtle">
   <div class="d-flex align-items-center justify-content-between">
      <a href="{{ route('home') }}" class="logo d-flex align-items-center">
         <img src="{{ asset('/img/logo.png') }}" alt="">
         <span class="d-none d-lg-block">NewAdmin</span>
      </a>
      <i class="bi bi-list toggle-sidebar-btn"></i>
   </div>
   <nav class="header-nav ms-auto">
      <ul class="d-flex align-items-center">
         <li class="nav-item dropdown">
            <a class="nav-link nav-icon" href="#" data-bs-toggle="dropdown">
               <i class="far fa-bell"></i>
               <span class="badge bg-primary badge-number">4</span>
            </a>
            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow notifications"  style="max-height: 300px; overflow-y: auto;">
               <li class="dropdown-header">
                  Menampilkan Status Notifikasi
               </li>
               <li>
                  <hr class="dropdown-divider">
               </li>
               <li class="notification-item">
                  <i class="bi bi-exclamation-circle text-warning"></i>
                  <div>
                     <h4>Lorem Ipsum</h4>
                     <p>Quae dolorem earum veritatis oditseno</p>
                     <p>30 min. ago</p>
                  </div>
               </li>
               <li>
                  <hr class="dropdown-divider">
               </li>
               <li class="notification-item">
                  <i class="bi bi-x-circle text-danger"></i>
                  <div>
                     <h4>Atque rerum nesciunt</h4>
                     <p>Quae dolorem earum veritatis oditseno</p>
                     <p>1 hr. ago</p>
                  </div>
               </li>
               <li>
                  <hr class="dropdown-divider">
               </li>
               <li class="notification-item">
                  <i class="bi bi-check-circle text-success"></i>
                  <div>
                     <h4>Sit rerum fuga</h4>
                     <p>Quae dolorem earum veritatis oditseno</p>
                     <p>2 hrs. ago</p>
                  </div>
               </li>
               <li>
                  <hr class="dropdown-divider">
               </li>
               <li class="notification-item">
                  <i class="bi bi-info-circle text-primary"></i>
                  <div>
                     <h4>Dicta reprehenderit</h4>
                     <p>Quae dolorem earum veritatis oditseno</p>
                     <p>4 hrs. ago</p>
                  </div>
               </li>
               <li>
                  <hr class="dropdown-divider">
               </li>
               <li class="dropdown-footer">
               </li>
            </ul>
         </li>

         <li class="nav-item dropdown pe-3">
            <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
               <span class="d-none d-md-block dropdown-toggle ps-2">User Login</span>
            </a>
            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
               <li class="dropdown-header">
                  <h6>User Login</h6>
                  <span>Job Position</span>
               </li>
               <li>
                  <hr class="dropdown-divider">
               </li>
               <li>
                  <a class="dropdown-item d-flex align-items-center" href="users-profile.html">
                     <i class="bi bi-person"></i>
                     <span>My Profile</span>
                  </a>
               </li>
               <li>
                  <hr class="dropdown-divider">
               </li>
               <li>
                  <a class="dropdown-item d-flex align-items-center" href="users-profile.html">
                     <i class="bi bi-gear"></i>
                     <span>Account Settings</span>
                  </a>
               </li>
               <li>
                  <hr class="dropdown-divider">
               </li>
               <li>
                  <a class="dropdown-item d-flex align-items-center" href="#">
                     <i class="bi bi-box-arrow-right"></i>
                     <span>Sign Out</span>
                  </a>
               </li>
            </ul>
         </li>
      </ul>
   </nav>
</header>