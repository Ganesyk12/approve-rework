<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');
        if (Auth::attempt($credentials)) {
            // Jika login berhasil, redirect ke halaman utama
            return redirect()->intended('home');
        }
        // Jika gagal, kembalikan ke form login dengan pesan kesalahan
        return redirect()->back()->withErrors(['username' => 'Username atau password salah.']);
    }
}
