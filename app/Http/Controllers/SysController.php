<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use App\Models\Auth;
use App\Models\Approval;
use App\Models\ChangeSortir;
use App\Models\Check;
use App\Models\Result;
use App\Models\Rework;
use App\Models\Sortir;
use Carbon\Carbon;

class SysController extends Controller
{
    public function reworks()
    {
        $reworks = Rework::all();
        return view('rework.dashboard', compact('reworks'));
    }
    public function getData()
    {
        $searchFields = ['hdrid', 'part_number', 'part_name', 'cust_name', 'cust_name2', 'problem', 'occ_place', 'occ_date', 'ng_qty', 'defect_lot', 'area', 'sketch1', 'deskripsi1', 'sketch2', 'deskripsi2', 'skecth3', 'deskripsi3', 'checking_method', 'attach_ik', 'marking_photo', 'marking', 'sortir', 'perubahan_sortir', 'request_to', 'nama_spv', 'request_by', 'checked_by', 'approved_by', 'jabatan_request', 'jabatan_checked', 'jabatan_approved', 'status_sortir', 'nik'];
        $result = Rework::get($searchFields);
        if ($result->isEmpty()) {
            return response()->json([
                'status' => 'No data found',
                'data' => [],
            ], 200);
        }
        return response()->json($result);
    }

    // ====================== getDataWhere

    public function ajax_getbyhdrid(Request $request)
    {
        $hdrid = $request->input('hdrid');
        $data = Rework::where('hdrid', $hdrid)->first();
        return response()->json($data);
    }

    // =============================================== ( Add, Update, Delete)
    public function addProduct(Request $request)
    {
        $rework = new Rework();
        $rework->part_number    = $request->part_number;
        $rework->part_name      = $request->part_name;
        $rework->cust_name      = $request->cust_name;
        $rework->cust_name2     = $request->cust_name2;
        $rework->problem        = $request->problem;
        $rework->occ_place      = $request->occ_place;
        $rework->occ_date       = $request->occ_date;
        $rework->ng_qty         = $request->ng_qty;
        $rework->defect_lot     = $request->defect_lot;
        $rework->area           = $request->area;
        $rework->sketch1        = $request->sketch1;
        $rework->sketch2        = $request->sketch2;
        $rework->sketch3        = $request->sketch3;
        $rework->deskripsi1     = $request->deskripsi1;
        $rework->deskripsi2     = $request->deskripsi2;
        $rework->deskripsi3     = $request->deskripsi3;
        $rework->checking_method   = $request->checking_method;
        $rework->attach_ik      = $request->attach_ik;
        $rework->marking_photo  = $request->marking_photo;
        $rework->marking        = $request->marking;
        $rework->sortir         = $request->sortir;
        $rework->perubahan_sortir  = $request->perubahan_sortir;
        $rework->perubahan_sortir  = $request->perubahan_sortir;
        $rework->nik            = $request->nik;
        $rework->request_to     = $request->request_to;
        $rework->nama_recipient = $request->nama_recipient;
        $rework->request_by     = $request->request_by;
        $rework->checked_by     = $request->checked_by;
        $rework->approved_by    = $request->approved_by;
        $rework->role_request   = $request->role_request;
        $rework->role_checked   = $request->role_checked;
        $rework->role_approved  = $request->role_approved;
        $rework->reject_requestspv  = $request->reject_requestspv;
        $rework->reject_requestmgr  = $request->reject_requestmgr;
        $rework->sortir_requestby   = $request->sortir_requestby;
        $rework->sortir_requestby   = $request->sortir_requestby;
        $rework->sortir_approvedby  = $request->sortir_approvedby;

        $rework->save();
        return response()->json(['status' => "success saved"]);
        $rework = Rework::create($request->all());
        return response()->json($rework);
    }

    public function deleteProduct(Request $request)
    {
        Rework::find($request->hdrid)->delete();
        return response()->json(['status' => "success deleted"]);
    }

    public function ajaxAdd(Request $request)
    {
        // ********************* 0. Generate nomor transaksi  *********************          
        $mdate = "REQ" . Carbon::now()->format('Ym');   // Gunakan Carbon untuk tanggal
        $hdrid2 = Rework::where('hdrid', 'like', "$mdate%")
            ->orderBy('hdrid', 'desc')
            ->first();
        if (is_null($hdrid2)) {
            $hdrid3 = $mdate . "001";   // Jika belum ada, mulai dengan 001
        } else {
            $lastId = (int) substr($hdrid2->hdrid, 3);  // Ambil nomor urut terakhir
            $hdrid3 = "REQ" . ($lastId + 1);    // Naikkan 1 level
        }
        $hdrid = $hdrid3;
        // ********************* 1. Set hdrid  *********************
        $post_data2 = ['hdrid' => $hdrid];
        // ********************* 2. Transaction date  *********************
        // $post_data3 = ['transaction_date' => Carbon::now()->format('Y-m-d')];
        // ********************* 3. Collect all data post *********************
        $post_data = $request->all();  // Mengambil semua input dari request
        $occdate = $request->input('occ_date', null);  // Mendapatkan data 'occ_date'
        $post_data5 = ['occ_date' => $occdate];  // Default ke null jika tidak ada
        // ********************* 4. Merge data post *********************
        $post_datamerge = array_merge($post_data, $post_data2, $post_data5);
        // ********************* 5. Simpan data  *********************
        Rework::create($post_datamerge);
        // ********************* 6. Upload file jika ada  *********************   
        $this->uploadFiles($request, $hdrid);
        // ********************* 7. Return response  *********************
        return Response::json([
            'status' => 'success',
            'message' => 'Data saved successfully.',
            'hdrid' => $hdrid,
        ]);
    }

    private function uploadFiles(Request $request, $hdrid)
    {
        $fileFields = ['sketch1', 'sketch2', 'sketch3', 'attach_ik', 'marking_photo'];
        // Mendefinisikan folder untuk setiap jenis file
        $fileFolders = [
            'sketch1' => 'public/uploads/sketch',
            'sketch2' => 'public/uploads/sketch',
            'sketch3' => 'public/uploads/sketch',
            'marking_photo' => 'public/uploads/sketch',
            'attach_ik' => 'public/upload/doc',
        ];

        // Limit ukuran file dalam kilobyte (misalnya, 1 MB = 1024 KB)
        $fileLimits = [
            'sketch1' => 1024,  // 1 MB
            'sketch2' => 1024,  // 1 MB
            'sketch3' => 1024,  // 1 MB
            'marking_photo' => 2048,  // 2 MB
            'attach_ik' => 5120,  // 5 MB
        ];

        // Jenis file yang diizinkan
        $fileTypes = [
            'sketch1' => ['jpg', 'jpeg', 'png', 'tiff'],
            'sketch2' => ['jpg', 'jpeg', 'png', 'tiff'],
            'sketch3' => ['jpg', 'jpeg', 'png', 'tiff'],
            'marking_photo' => ['jpg', 'jpeg', 'png', 'tiff'],
            'attach_ik' => ['pdf', 'xls', 'xlsx'],
        ];

        // Melakukan loop pada setiap field file
        foreach ($fileFields as $field) {
            if ($request->hasFile($field)) {
                $file = $request->file($field);

                // Periksa ukuran file
                if ($file->getSize() / 1024 > $fileLimits[$field]) {
                    throw new \Exception("File '{$field}' terlalu besar. Batas maksimal: " . $fileLimits[$field] . " KB.");
                }

                // Periksa jenis file
                $fileExtension = $file->getClientOriginalExtension();
                if (!in_array($fileExtension, $fileTypes[$field])) {
                    throw new \Exception("Jenis file '{$field}' tidak diizinkan. Jenis file yang diizinkan: " . implode(", ", $fileTypes[$field]));
                }

                // Simpan file ke folder yang sesuai
                $folder = $fileFolders[$field];
                Storage::putFileAs($folder, $file, "{$hdrid}_{$field}.{$fileExtension}");
            }
        }
    }

    public function ajaxAddSortir(Request $request)
    {
        // ********************* 0. Generate nomor transaksi  *********************          
        $mdate = "SOR" . Carbon::now()->format('Ym');
        $lastSortir = Sortir::where('id_sortir', 'like', "$mdate%")
            ->orderBy('id_sortir', 'desc')
            ->first();
        if (is_null($lastSortir)) {
            $id_sortir = $mdate . "001";
        } else {
            $lastId = intval(substr($lastSortir->id_sortir, 3));
            $id_sortir = "SOR" . ($lastId + 1);
        }
        // ********************* 1. Set id_sortir  *********************
        $post_data2 = ['id_sortir' => $id_sortir];
        // ********************* 2. Transaction date  *********************
        $post_data3 = ['transaction_date' => Carbon::now()->format('Y-m-d')];
        // ******************** 3. Collect all data post *********************     
        $post_data = $request->all();
        // Ubah tanggal customer dan Yusen
        $tgl_customer = [
            'tgl_berangkat_customer' => $request->input('tgl_berangkat_customer', null),
            'tgl_datang_customer' => $request->input('tgl_datang_customer', null),
        ];
        $tgl_yusen = [
            'tgl_berangkat_yusen' => $request->input('tgl_berangkat_yusen', null),
            'tgl_datang_yusen' => $request->input('tgl_datang_yusen', null),
        ];
        // ********************* 4. Merge data post *********************        
        $post_datamerge = array_merge($post_data, $post_data2, $post_data3, $tgl_customer, $tgl_yusen);
        // ********************* 5. Simpan data ke database *********************
        Sortir::create($post_datamerge);
        // ********************* 6. Simpan data tambahan *********************   
        $post_result = [
            'part_number' => $request->input('part_number'),
            'lot_produksi' => $request->input('lot_produksi'),
            'id_request' => $request->input('id_request'),
            'id_sortir' => $id_sortir,
        ];
        // $this->ajaxAddResult($post_result);
        // ********************* 7. Return JSON Response *********************   
        return response()->json([
            'status' => 'success saved',
            'id_sortir' => $id_sortir,
        ]);
    }

    public function ajaxAddResult(Request $request)
    {
        // ********************* 0. Generate nomor transaksi  *********************          
        $mdate = "RES" . Carbon::now()->format('Ym');
        $lastResult = Result::where('id_result', 'like', "$mdate%")
            ->orderBy('id_result', 'desc')
            ->first();
        if (is_null($lastResult)) {
            $id_result = $mdate . "001";
        } else {
            $lastId = intval(substr($lastResult->id_result, 3));
            $id_result = "RES" . ($lastId + 1);
        }
        // ********************* 1. Set id_result  *********************
        $post_data2 = ['id_result' => $id_result];
        // ********************* 2. Transaction date  *********************
        $post_data3 = ['transaction_date' => Carbon::now()->format('Y-m-d')];
        // ********************* 3. Collect all data post *********************
        $post_datamerge = array_merge($request->all(), $post_data2, $post_data3);
        // ********************* 4. Simpan data ke database *********************
        Result::create($post_datamerge);
        // ********************* 5. Return response dalam format JSON *********************   
        return response()->json([
            'status' => 'success save',
            'id_result' => $id_result  // Memberikan informasi ID yang dihasilkan
        ]);
    }

    public function ajaxAddCheck(Request $request)
    {
        // ********************* 0. Generate nomor transaksi  ********************
        $mdate = "CHK" . Carbon::now()->format('Ym');
        $lastCheck = Check::where('id_check', 'like', "$mdate%")
            ->orderBy('id_check', 'desc')
            ->first();
        $id_check = is_null($lastCheck) ? $mdate . "001" : "CHK" . (intval(substr($lastCheck->id_check, 3)) + 1);
        // ********************* 1. Set id_check  **********************************
        $post_data2 = ['id_check' => $id_check];
        // ********************* 2. Transaction date  ************************
        $post_data3 = ['transaction_date' => Carbon::now()->format('Y-m-d')];
        // ******************** 3. Collect all data post *********************
        $post_data = $request->all();
        $msg = "success save";
        // ********************* 4. Merge data post ***************************        
        $post_datamerge = array_merge($post_data, $post_data2, $post_data3);
        // ********************* 5. Simpan data ke database ***************************
        Check::create($post_datamerge);
        // ********************* 6. Return response dalam format JSON *********************   
        return Response::json([
            'status' => $msg,
            'id_check' => $id_check,
        ]);
    }

    public function ajaxAddApproval(Request $request)
    {
        // ********************* 0. Generate nomor transaksi  *********************
        $mdate = "APR" . Carbon::now()->format('Ym');
        $lastApproval = Approval::where('id_approval', 'like', "$mdate%")
            ->orderBy('id_approval', 'desc')
            ->first();
        $id_approval = is_null($lastApproval) ? $mdate . "001" : "APR" . (intval(substr($lastApproval->id_approval, 3)) + 1);
        // ********************* 1. Set id_approval  *********************
        $post_data2 = ['id_approval' => $id_approval];
        // ********************* 2. Transaction date  *********************
        $post_data3 = ['transaction_date' => Carbon::now()->format('Y-m-d')];
        // ******************** 3. Collect all data post *********************
        $post_data = $request->all();
        // ********************* 4. Merge data post *********************        
        $post_datamerge = array_merge($post_data, $post_data2, $post_data3);
        // ********************* 5. Simpan data ke database *********************
        Approval::create($post_datamerge); // Simpan ke database menggunakan Eloquent
        // ********************* 6. Return response dalam format JSON *********************
        return Response::json([
            'status' => 'success save',
            'id_approval' => $id_approval,
        ]);
    }

    public function ajaxAddPerubahan(Request $request)
    {
        // ********************* 0. Generate nomor transaksi  *********************
        $mdate = "CHG" . Carbon::now()->format('Ym');
        $lastChange = ChangeSortir::where('id_change', 'like', "$mdate%")
            ->orderBy('id_change', 'desc')
            ->first();
        $id_change = is_null($lastChange) ? $mdate . "001" : "CHG" . (intval(substr($lastChange->id, 3)) + 1);
        // ********************* 1. Set id  *********************
        $post_data2 = ['id_change' => $id_change];
        // ********************* 2. Transaction date  *********************
        $post_data3 = ['transaction_date' => Carbon::now()->format('Y-m-d')];
        // ******************** 3. Collect all data post *********************     
        $post_data = $request->all();
        $msg = "success save";
        // Ubah nilai tgl_perubahan berdasarkan input POST
        $tgl_perubahan_sortir = $request->input('tgl_perubahan');
        $tgl_perubahan = ['tgl_perubahan' => $tgl_perubahan_sortir ? $tgl_perubahan_sortir : null];
        // ********************* 4. Merge data post *********************
        $post_datamerge = array_merge($post_data, $post_data2, $post_data3, $tgl_perubahan);
        // ********************* 5. Simpan data ke database *********************
        ChangeSortir::create($post_datamerge);
        // ********************* 6. Return response dalam format JSON *********************
        return Response::json([
            'status' => $msg,
            'id_change' => $id_change,
        ]);
    }
}
