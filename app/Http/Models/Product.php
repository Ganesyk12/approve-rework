<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Rework extends Model
{
    protected $fillable = [
        'hdrid',
        'created_at',
        'nik',
        'part_number',
        'part_name',
        'cust_name',
        'anothercust_name',
        'problem',
        'occ_place',
        'occ_date',
        'ng_qty',
        'defect_lot',
        'area',
        'sketch1',
        'sketch2',
        'sketch3',
        'deskripsi1',
        'deskripsi2',
        'deskripsi3',
        'checking_method',
        'attach_ik',
        'marking_photo',
        'marking',
        'sortir',
        'perubahan_sortir',
        'request_to',
        'nama_recipient',
        'request_by',
        'checked_by',
        'approved_by',
        'role_request',
        'role_checked',
        'role_approved',
        'reject_requestspv',
        'reject_requestmgr',
        'sortir_requestby',
        'sortir_checkedby',
        'sortir_approvedby',
        'status_sortir',
    ];
}

Class Sortir extends Model {

    protected $fillable = [
        'id_sortir',
        'created_at',
        'id_request',
        'part_number',
        'lot_produksi',
        'customer',
        'masalah_stok',
        'masalah_customer',
        'tgl_berangkat_cust',
        'tgl_datang_cust',
        'masalah_yusen',
        'tgl_berangkat_yusen',
        'tgl_datang_yusen',
        'qty',
    ];

    public function request(){
        return $this->belongsTo(Rework::class, 'hdrid');
    }
}

Class ChangeSortir extends Model {

    protected $fillable = [
        'id_change',
        'created_at',
        'id_request',
        'tgl_perubahan',
        'jenis_perubahan',
        'check_spv',
        'check_mgr',
    ];

    public function request(){
        return $this->belongsTo(Rework::class, 'hdrid');
    }
}

Class Result extends Model {
    protected $fillable = [
        'id_result',
        'created_at',
        'id_request',
        'id_sortir',
        'result_date',
        'start_hour',
        'finish_hour',
        'inspector',
        'part_number',
        'lot_produksi',
        'qty_total',
        'qty_ok',
        'qty_ng',
        'aktual_ok',
        'aktual_ng',
        'remarks',
    ];

    public function request(){
        return $this->belongsTo(Rework::class, 'hdrid');
    }

    public function sortir(){
        return $this->belongsTo(Sortir::class, 'id_sortir');
    }
}

Class Check extends Model {
    protected $fillable = [
        'id_check',
        'created_at',
        'id_request',
        'data_ok',
        'data_ng',
        'aktual_ok',
        'aktual_ng',
        'keterangan',
    ];

    public function request(){
        return $this->belongsTo(Rework::class, 'hdrid');
    }
}

Class Approval extends Model {
    protected $fillable = [
        'trxid',
        'id_request',
        'created_at',
        'nik',
        'name',
        'department_code',
        'department_name',
        'office_email',
        'position_code',
        'position_name',
        'date_approve',
    ];

    public function request(){
        return $this->belongsTo(Rework::class, 'hdrid');
    }

}

Class Menu extends Model {
    protected $fillable = [
        'id_menu',
        'created_at',
        'controller',
        'description',
        'icon',
    ];
}

Class Auth extends Model {}

