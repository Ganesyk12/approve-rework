<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}

class Role extends Model
{
    protected $fillable = [
        'id_role',
        'created_at',
        'role_name',
        'description',
        'code',
    ];
}

class RoleAkses extends Model
{
    protected $fillable = [
        'id_access',
        'created_at',
        'id_role',
        'id_menu',
        'allow_add',
        'allow_edit',
        'allow_delete',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class, 'id_role');
    }

    public function menu()
    {
        return $this->belongsTo(Menu::class, 'id_menu');
    }
}

Class Customer extends Model{}
Class Superior extends Model{}