<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Rework extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rework', function (Blueprint $table) {
            $table->string('hdrid');
            $table->string('nik')->nullable();
            $table->string('part_number')->nullable();
            $table->string('part_name')->nullable();
            $table->string('cust_name')->nullable();
            $table->string('anothercust_name')->nullable();
            $table->string('problem')->nullable();
            $table->string('occ_place')->nullable();
            $table->date('occ_date')->nullable();
            $table->integer('ng_qty')->nullable();
            $table->string('defect_lot')->nullable();
            $table->string('area')->nullable();
            $table->string('sketch1')->nullable();
            $table->string('sketch2')->nullable();
            $table->string('sketch3')->nullable();
            $table->text('deskripsi1')->nullable();
            $table->text('deskripsi2')->nullable();
            $table->text('deskripsi3')->nullable();
            $table->string('checking_method')->nullable(); 
            $table->string('attach_ik')->nullable();
            $table->string('marking_photo')->nullable();
            $table->text('marking')->nullable();
            $table->string('sortir')->nullable();
            $table->string('perubahan_sortir')->nullable();
            $table->string('request_to')->nullable();
            $table->string('nama_recipient')->nullable();
            $table->string('request_by')->nullable();
            $table->string('checked_by')->nullable();
            $table->string('approved_by')->nullable();
            $table->string('role_request')->nullable();
            $table->string('role_checked')->nullable();
            $table->string('role_approved')->nullable();
            $table->string('reject_requestspv')->nullable();
            $table->string('reject_requestmgr')->nullable();
            $table->string('sortir_requestby')->nullable();
            $table->string('sortir_checkedby')->nullable();
            $table->string('sortir_approvedby')->nullable();
            $table->string('status_sortir')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rework');
    }
}
