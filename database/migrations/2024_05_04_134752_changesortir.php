<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Changesortir extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('changesortir', function (Blueprint $table) {
            $table->string('id_change');
            $table->string('id_request');
            $table->timestamp('tgl_perubahan')->nullable();
            $table->string('jenis_perubahan')->nullable();
            $table->string('check_spv')->nullable();
            $table->string('check_mgr')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('changesortir');
    }
}
