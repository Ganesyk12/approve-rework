<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Roleakses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_akses', function (Blueprint $table) {
            $table->string('id_access');
            $table->string('id_role');
            $table->string('id_menu');
            $table->string('allow_add')->nullable();
            $table->string('allow_edit')->nullable();
            $table->string('allow_delete')->nullable();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_akses');
    }
}
