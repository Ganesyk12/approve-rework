<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Sortir extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sortir', function (Blueprint $table) {
            $table->string('id_sortir');
            $table->string('id_request');
            $table->string('part_number')->nullable();
            $table->string('lot_produksi')->nullable();
            $table->string('customer')->nullable();
            $table->string('masalah_stok')->nullable();
            $table->string('masalah_customer')->nullable();
            $table->date('tgl_berangkat_cust')->nullable();
            $table->date('tgl_datang_cust')->nullable();
            $table->string('masalah_yusen')->nullable();
            $table->date('tgl_berangkat_yusen')->nullable();
            $table->date('tgl_datang_yusen')->nullable();
            $table->string('perlu_info');
            $table->timestamps();  // Kolom created_at dan updated_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sortir');
    }
}
