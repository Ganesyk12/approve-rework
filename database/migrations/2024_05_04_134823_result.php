<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Result extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result', function (Blueprint $table) {
            $table->string('id_result');
            $table->string('id_request');
            $table->string('id_sortir');
            $table->date('result_date')->nullable();
            $table->time('start_hour')->nullable();
            $table->time('finish_hour')->nullable();
            $table->string('inspector')->nullable();
            $table->string('part_number')->nullable();
            $table->string('lot_produksi')->nullable();
            $table->integer('qty_total')->nullable();
            $table->integer('qty_ok')->nullable();
            $table->integer('qty_ng')->nullable();
            $table->integer('aktual_ok')->nullable();
            $table->integer('aktual_ng')->nullable();
            $table->text('remarks')->nullable(); 
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result');
    }
}
